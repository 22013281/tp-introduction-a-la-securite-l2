#include<stdlib.h>
struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

list_t list_empty(){
	
	return NULL;
}

int list_is_empty(list_t l){
	
	return l==NULL;
}

list_t list_push(list_t l, void* x){
	
	list_t N = malloc(sizeof(struct cell_t));
	N->val = x;
	N-> next = l;
	if (list_is_empty(l))		
		N->id = 1;
	else
		N->id = l->id + 1;
		
	return N ;
	
}

list_t list_tail(list_t l){
	
	return l->next;
}

void* list_pop(list_t* l){ 
	
	list_t tmp= (*l)->next;
	void * v = (*l)->val;
	free(*l);
	*l = tmp;
	return v;
	
}

void* list_top(list_t l){

	return l->val;
	
}

void list_destroy(list_t l, void (*free_void)(void*)){
	while(!list_is_empty(l))
		free_void(list_pop(&l));
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
	
	if(list_is_empty(l))
		return NULL;
	
	while( ! list_is_empty(l)){
		
		
		if((eq) (l->val ,x))
			return l->val;
			
		else
			l=l->next;
		
	}
	return NULL;
}

unsigned long int list_len(list_t l){
	
	unsigned long int taille;
	if (list_is_empty(l))
		return 0;
	else
		return l->id;
	
}


